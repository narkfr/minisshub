# MinIssHub

MinIssHub is a minimalist Github issues search engine.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

The project is published on [nark.fr/minisshub](http://nark.fr/minisshub/)

## Purposes

I use Create React to focus on the coding style, and avoid a long tooling configuration. A webpack configuration is specific for each project, and this exercice doesn't need a specific setup.

I choose to use CSS instead of Sass or Less for 2 reasons:

- Avoid to `eject` the configuration from Create React App to add a specific loader
- I think a clear and clean css file are as important as using fancy and powerfull tools like Sass or Less.

Bootstrap is used as CSS framework.

The **Redux** is used as one and only one source of truth to fetch and store repositories and issues, and persist data during navigation.

## Install

__Requirements__


You'll need to have Node >=6 on your local development machine, and Yarn.

``yarn install``: install the project
``yarn start``: run the project. It will open a browser with your project.

All default Create React App scripts are supported, see [Create React App README](https://github.com/facebook/create-react-app/blob/next/README.md) for full documentation

## i18n

Internationalisation is managed by [react-intl] (https://www.npmjs.com/package/react-intl) and a [babel plugin](https://github.com/yahoo/babel-plugin-react-intl) to extract string messages from React components.

That's a minimalist i18n feature, and it will probably need improvements, but I didn't want to `eject` the create react app configuration to keep the exercice simple.

### Workflow

``yarn extract:messages``: extract the messages defined in every defineMessages method of react-intl inside our src directory

``yarn manage:translations``: create a separate messages file for each language

See this [Medium Post](https://medium.com/@shalkam/create-react-app-i18n-the-easy-way-b05536c594cb) for full explanation.
