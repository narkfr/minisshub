import React, { Component } from 'react'
import { injectIntl } from "react-intl"

import './Header.css'

import headerMessages from '../../i18n/header'

import LanguagesSwitcher from './LanguagesSwitcher'


class Header extends Component {
    render() {
        const {intl:{formatMessage}} = this.props
        return (
            <header className="header">
                <h1 className="header-title">MinIssHub</h1>
                <h2 className="header-subtitle hidden-xs hidden-sm">{formatMessage(headerMessages.subTitle)}</h2>
                <LanguagesSwitcher />
            </header>
        )
    }
}

export default injectIntl(Header)
