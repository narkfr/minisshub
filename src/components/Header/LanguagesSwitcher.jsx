import React, { Component } from 'react'

import './LanguagesSwitcher.css'

class LanguagesSwitcher extends Component {
    render() {
        return (
            <ul className="header-languages">
                <li className="header-languages-item"><a href="?locale=fr">Français</a></li>
                <li className="header-languages-item"><a href="?locale=en">English</a></li>
            </ul>
        )
    }
}

export default LanguagesSwitcher
