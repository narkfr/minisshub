import React from 'react'
import { Glyphicon } from 'react-bootstrap'
import {connect} from 'react-redux'

import './RepositoriesList.css'

import { removeRepository } from '../../actions/repositories'

const RepositoriesList = ({repositoriesList, removeRepository}) =>
    <ul className="list-group">
        {repositoriesList.map(
            repository =>
                <li key={repository} className="list-group-item list-group-item-action">
                    <a
                        className="repositoriesList-removeButton"
                        onClick={() => removeRepository(repositoriesList.indexOf(repository))}>
                        <Glyphicon glyph="remove" />
                    </a>
                    <a
                        href={`https://www.github.com/${repository}`}
                        target="_blank"
                    >
                        {repository}
                    </a>
                </li>
            )
        }
    </ul>

export default connect(
    // mapStateToProps
    null,
    // mapDispatchToProps
    {removeRepository}
)(RepositoriesList)
