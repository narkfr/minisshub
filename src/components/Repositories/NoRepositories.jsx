import React from 'react'
import { injectIntl } from "react-intl"

import './NoRepositories.css'

import repositoriesMessages from '../../i18n/repositories'

import AddRepository from './AddRepository'

const NoRepositories = ({intl:{formatMessage}}) => <div className="noRepositories">
    <h3>{formatMessage(repositoriesMessages.noRepositories)}</h3>
    <AddRepository />
</div>

export default injectIntl(NoRepositories)
