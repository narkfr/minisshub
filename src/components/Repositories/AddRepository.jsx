import React, { Component, Fragment } from 'react'
import {
    Button,
    ControlLabel,
    FormControl,
    FormGroup,
    Glyphicon,
    Modal
} from 'react-bootstrap'
import { injectIntl } from 'react-intl'
import { connect } from 'react-redux'
import Spinner from 'react-spinkit'

import './AddRepository.css'

import { fetchRepository } from '../../actions/repositories'

import repositoriesMessages from '../../i18n/repositories'

class AddRepository extends Component {
    state = {
        isModalOpen: false,
        isValueEmpty: false
    }

    openModal = () => {
        this.setState({ isModalOpen: true })
    }

    closeModal = () => {
        this.setState({ isModalOpen: false })
    }

    addRepository = (value) => {
        value
            ? this.props.fetchRepository(value)
            : this.setState({ isValueEmpty: true })
    }

    handleChange = (event) => {
        event.target.value && this.setState({ isValueEmpty: false })
        this.setState({ value: event.target.value })
    }

    handleKeyDown = (event) => {if (event.keyCode === 13) {
        event.preventDefault()
        this.addRepository(this.state.value)
    }}

    componentDidUpdate = (prevProps, prevState) => {
        // If the repositories have changed, we close the Modal
        prevProps.repositories.repositories !== this.props.repositories.repositories && this.closeModal()
    }

    render() {
        const { isModalOpen, isValueEmpty } = this.state
        const { intl:{formatMessage} } = this.props
        const {
            noRepository,
            requestRepositoryError,
            isFetchingRepository
        } = this.props.repositories
        const error =
            isValueEmpty && formatMessage(repositoriesMessages.noValue) ||
            noRepository && formatMessage(repositoriesMessages.noRepository) ||
            requestRepositoryError && formatMessage(repositoriesMessages.requestRepositoryError) ||
            undefined

        return (
            <Fragment>
                <div className="addRepository">
                    <Button bsStyle="primary" onClick={() => this.openModal()}>{formatMessage(repositoriesMessages.addRepositoryButton)} <Glyphicon glyph="plus" /></Button>
                </div>
                <Modal show={isModalOpen} onHide={() => this.closeModal()}>
                    <Modal.Header closeButton>
                        <Modal.Title>{formatMessage(repositoriesMessages.addRepositoryTitle)}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form onSubmit={() => this.addRepository(this.state.value)}>
                            <FormGroup>
                                <ControlLabel>{formatMessage(repositoriesMessages.addRepositoryLabel)}</ControlLabel>
                                <FormControl
                                    type="text"
                                    placeholder="user/repository"
                                    onChange={this.handleChange}
                                    onKeyDown={this.handleKeyDown}
                                />

                                {
                                    error && <div className="alert alert-warning">{error}</div>
                                }
                            </FormGroup>
                            <div className="addRepository-submitButton">
                            {
                                isFetchingRepository
                                ? <Spinner name="circle" />
                                : <Button bsStyle="primary" onClick={() => this.addRepository(this.state.value)}>
                                    {formatMessage(repositoriesMessages.addRepositoryButton)}
                                </Button>
                            }
                            </div>
                        </form>
                    </Modal.Body>
                    <Modal.Footer />
                </Modal>
            </Fragment>
        )
    }
}

export default injectIntl(connect(
    // mapStateToProps
    (state) => {
        return { repositories: state.repositories || [] }
    },
    // mapDispatchToProps
    {fetchRepository}
)(AddRepository))
