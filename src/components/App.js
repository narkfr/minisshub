import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import { connect } from 'react-redux'

import '../styles/sidebar.css'

import Header from './Header/Header'
import Issues from './Issues/Issues'
import AddRepository from './Repositories/AddRepository'
import NoRepositories from './Repositories/NoRepositories'
import RepositoriesList from './Repositories/RepositoriesList'

class App extends Component {
    render() {
        const repositoriesList = this.props.repositories.repositories
        return (
            <div className="App">
                <Header />
                <main className="main">
                    {
                        repositoriesList.length
                        ? <Grid>
                            <Row>
                                <Col md={3} className="sidebar">
                                    <AddRepository />
                                    <RepositoriesList
                                        repositoriesList={repositoriesList}
                                    />
                                </Col>
                                <Col md={9}>
                                    <Issues />
                                </Col>
                            </Row>

                        </Grid>
                        : <NoRepositories />
                    }
                </main>
            </div>
        )
    }
}


export default connect(
    // mapStateToProps
    (state) => {
        return { repositories: state.repositories || [] }
    },
    // mapDispatchToProps
    null
)(App)
