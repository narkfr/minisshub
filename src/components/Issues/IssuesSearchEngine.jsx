import React, { Component } from 'react'
import {
    Button,
    ControlLabel,
    FormControl,
    FormGroup
} from 'react-bootstrap'
import { injectIntl } from "react-intl"
import { connect } from 'react-redux'

import './IssuesSearchEngine.css'

import { fetchIssues } from '../../actions/issues'

import issuesMessages from '../../i18n/issues'

class IssuesSearchEngine extends Component {

    state = {
        isValueEmpty: false
    }

    handleChange = (event) => {
        event.target.value && this.setState({ isValueEmpty: false })
        this.setState({ value: event.target.value })
    }

    handleKeyDown = (event) => {if (event.keyCode === 13) {
        event.preventDefault()
        this.searchIssues()
    }}

    searchIssues = () => {
        const { repositories, fetchIssues } = this.props
        this.state.value
            ? fetchIssues(repositories, this.state.value)
            : this.setState({ isValueEmpty: true })
    }

    render() {
        const { intl:{formatMessage} } = this.props
        const { isValueEmpty } = this.state
        return (
            <form className="issuesSearchEngine">
                <FormGroup
                    controlId="formBasicText"
                >
                    <ControlLabel>{formatMessage(issuesMessages.search)}</ControlLabel>
                    <FormControl
                        type="text"
                        placeholder={formatMessage(issuesMessages.searchPlaceHolder)}
                        onChange={this.handleChange}
                        onKeyDown={this.handleKeyDown}
                    />
                </FormGroup>
                {
                    isValueEmpty && <div className="alert alert-warning">{formatMessage(issuesMessages.errorValueNeeded)}</div>
                }
                <Button onClick={() => this.searchIssues()}>{formatMessage(issuesMessages.searchButton)}</Button>
            </form>
        )
    }
}

export default injectIntl(connect(
    // mapStateToProps
    (state) => {
        return { repositories: state.repositories || [] }
    },
    // mapDispatchToProps
    { fetchIssues }
)(IssuesSearchEngine))
