import React, { Component, Fragment } from 'react'
import { injectIntl } from "react-intl"
import { connect } from 'react-redux'
import Spinner from 'react-spinkit'

import issuesMessages from '../../i18n/issues'

class IssuesList extends Component {
    render() {
        const { issues, intl:{formatMessage} } = this.props
        return (
            <Fragment>
                {
                    issues.isFetchingIssues
                    ? <Spinner name="circle" />
                    : issues.issues.length
                        ? <ul className="list-group">
                            {issues.issues.map(
                                issue =>
                                    <li className="list-group-item d-flex justify-content-between align-items-center">
                                        <span className="badge badge-pill">
                                            <b>{issue.state}</b>
                                        </span>
                                        <span className="badge badge-pill">
                                            {issue.repository_url.substring(issue.repository_url.lastIndexOf("/") + 1, issue.repository_url.length)}
                                        </span>
                                        <a href={issue.url} target="_blank">{issue.title}</a>
                                    </li>
                                )}
                        </ul>
                        : issues.noIssues && <div className="alert alert-warning">{formatMessage(issuesMessages.noIssues)}</div>
                }
            </Fragment>

        )
    }
}

export default injectIntl(connect(
    // mapStateToProps
    (state) => {
        return { issues: state.issues || [] }
    },
    // mapDispatchToProps
    null
)(IssuesList))
