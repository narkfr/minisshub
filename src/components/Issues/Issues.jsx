import React, { Fragment } from 'react'

import IssuesList from './IssuesList'
import IssuesSearchEngine from './IssuesSearchEngine'

const Issues = () =>
    <Fragment>
        <IssuesSearchEngine />
        <IssuesList />
    </Fragment>

export default Issues
