export const REQUEST_ISSUES = 'REQUEST_ISSUES'
export const requestIssues = () => ({
    type: REQUEST_ISSUES
})

export const REQUEST_ISSUES_ERROR = 'REQUEST_ISSUES_ERROR'
export const requestIssuesError = () => ({
    type: REQUEST_ISSUES_ERROR
})

export const RECEIVE_ISSUES = 'RECEIVE_ISSUES'
export const receiveIssues = (data) => ({
    type: RECEIVE_ISSUES,
    payload: data
})

export const CLEAR_ISSUES = 'CLEAR_ISSUES'
export const clearIssues = () => ({
    type: CLEAR_ISSUES
})

export const NO_ISSUES = 'NO_ISSUES'
export const noIssues = () => ({
    type: NO_ISSUES
})


export const fetchIssues = (repositories, value) => {
    return function (dispatch) {
        dispatch(requestIssues())
        dispatch(clearIssues())
        const repositoriesListQueryParamater = repositories.repositories.join('+repo:')
        fetch(`https://api.github.com/search/issues?q=${value}+in:title+repo:${repositoriesListQueryParamater}`)
            .then(response => response.json())
            .then(data => {
                data.total_count > 0
                ? dispatch(receiveIssues(data))
                : dispatch(noIssues())
            })
            .catch(error => {
                dispatch(requestIssuesError())
            })
    }
}
