export const REQUEST_REPOSITORY = 'REQUEST_REPOSITORY'
export const requestRepository = () => ({
    type: REQUEST_REPOSITORY
})

export const REQUEST_REPOSITORY_ERROR = 'REQUEST_REPOSITORY_ERROR'
export const requestRepositoryError = () => ({
    type: REQUEST_REPOSITORY_ERROR
})

export const RECEIVE_REPOSITORY = 'RECEIVE_REPOSITORY'
export const receiveRepository = (data) => ({
    type: RECEIVE_REPOSITORY,
    payload: data
})

export const DELETE_REPOSITORY = 'DELETE_REPOSITORY'
export const deleteRepository = (indexOfRepository) => ({
    type: DELETE_REPOSITORY,
    payload: indexOfRepository
})

export const NO_REPOSITORY = 'NO_REPOSITORY'
export const noRepository = () => ({
    type: NO_REPOSITORY
})

export const fetchRepository = (value) => {
    return function (dispatch) {
        dispatch(requestRepository())

        fetch(`https://api.github.com/search/repositories?q=${value}`)
            .then(response => response.json())
            .then(data => {
                data.total_count > 0
                    ? dispatch(receiveRepository(data.items[0].full_name))
                    : dispatch(noRepository())
            })
            .catch(error => {
                dispatch(requestRepositoryError())
            })
    }
}

export const removeRepository = (indexOfRepository) => {
    return async function (dispatch) {
        dispatch(deleteRepository(indexOfRepository))
    }
}
