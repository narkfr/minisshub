import {
    REQUEST_ISSUES,
    REQUEST_ISSUES_ERROR,
    RECEIVE_ISSUES,
    NO_ISSUES
} from '../actions/issues'

export function issues(state = {
    isFetchingIssues: false,
    issues: [],
    noIssues: false,
    requestIssuesError: false
}, {
    type,
    payload
}) {
    switch (type) {
        case REQUEST_ISSUES:
            return {
                ...state,
                isFetchingIssues: true
            }

        case REQUEST_ISSUES_ERROR:
            return {
                ...state,
                isFetchingIssues: false,
                requestIssuesError: true
            }

        case RECEIVE_ISSUES:
            return {
                ...state,
                isFetchingIssues: false,
                issues: payload.items,
                noIssues: false,
                requestIssuesError: false
            }

        case NO_ISSUES:
            return {
                ...state,
                isFetchingIssues: false,
                noIssues: true
            }

        default:
            return state
    }
}
