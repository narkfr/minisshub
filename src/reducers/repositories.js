import {
    REQUEST_REPOSITORY,
    REQUEST_REPOSITORY_ERROR,
    RECEIVE_REPOSITORY,
    DELETE_REPOSITORY,
    NO_REPOSITORY
} from '../actions/repositories'

export function repositories(state = {
    isFetchingRepository: false,
    repositories: [],
    noRepository: false,
    requestRepositoryError: false
}, {
    type,
    payload
}) {
    switch (type) {
        case REQUEST_REPOSITORY:
            return {
                ...state,
                isFetchingRepository: true
            }

        case REQUEST_REPOSITORY_ERROR:
            return {
                ...state,
                isFetchingRepository: false,
                requestRepositoryError: true
            }

        case RECEIVE_REPOSITORY:
            return {
                ...state,
                isFetchingRepository: false,
                repositories: [...state.repositories, payload],
                noRepository: false,
                requestRepositoryError: false
            }

        case DELETE_REPOSITORY:
            console.log(payload)
            return {
                ...state,
                isFetchingRepository: false,
                repositories: [
                    ...state.repositories.slice(0, payload),
                    ...state.repositories.slice(payload + 1)
                ]
            }

        case NO_REPOSITORY:
            return {
                ...state,
                isFetchingRepository: false,
                noRepository: true
            }

        default:
            return state
    }
}
