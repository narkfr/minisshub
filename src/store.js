import {applyMiddleware, combineReducers, createStore} from 'redux'
import thunk from 'redux-thunk'

import * as repositories from './reducers/repositories'
import * as issues from './reducers/issues'


const middleware = [thunk]

const store = createStore(
    combineReducers({
        ...repositories,
        ...issues
    }),
    applyMiddleware(...middleware)
)


export default store
