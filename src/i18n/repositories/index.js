import { defineMessages } from "react-intl";

const repositoriesMessages = defineMessages({
  noRepositories: {
    id: 'repositories.noRepositories',
    defaultMessage: 'First, add a repository to start your research.'
  },
  addRepositoryTitle: {
    id: 'repositories.addRepositoryTitle',
    defaultMessage: 'Add a repository to your list'
  },
  addRepositoryLabel: {
    id: 'repositories.addRepositoryLabel',
    defaultMessage: 'Add a repository with the format `user/repository`'
  },
  addRepositoryButton: {
    id: 'repositories.addRepositoryButton',
    defaultMessage: 'Add a repository'
  },
  noValue: {
    id: 'repositories.noValue',
    defaultMessage: 'Please enter a value.'
  },
  noRepository: {
    id: 'repositories.noRepository',
    defaultMessage: 'No Repository for this request'
  },
  requestRepositoryError: {
    id: 'repositories.requestRepositoryError',
    defaultMessage: 'Oops, something went wrong'
  },
})

export default repositoriesMessages
