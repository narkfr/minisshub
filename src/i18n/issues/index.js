import { defineMessages } from "react-intl";

const issuesMessages = defineMessages({
  noIssues: {
    id: 'issues.noIssues',
    defaultMessage: 'No issues for this research.'
  },
  search: {
      id: 'issues.search',
      defaultMessage: 'Search an issue title for your tracked repositories.'
  },
  searchPlaceHolder: {
    id: 'issues.searchPlaceHolder',
    defaultMessage: 'Issue title'
  },
  errorValueNeeded: {
    id: 'issues.errorValueNeeded',
    defaultMessage: 'Please enter a value.'
  },
  searchButton: {
    id: 'issues.searchButton',
    defaultMessage: 'Search'
  },
})

export default issuesMessages
