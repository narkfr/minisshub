import { defineMessages } from "react-intl";

const headerMessages = defineMessages({
  subTitle: {
    id: 'header.subTitle',
    defaultMessage: 'A minimalist Github Issues search engine'
  },
})

export default headerMessages
